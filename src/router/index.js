import Vue from 'vue';
import VueRouter from 'vue-router';
import Index from '../views/Index.vue';

Vue.use(VueRouter);

const routes = [
	{
		path: '/',
		name: 'Index',
		component: Index,
		meta: { title: 'Incio - Delicias Norte' }
	},
	{
		path: '/registro/',
		name: 'SignIn',
		// route level code-splitting
		// this generates a separate chunk (about.[hash].js) for this route
		// which is lazy-loaded when the route is visited.
		component: () =>
			import(/* webpackChunkName: "about" */ '../views/SignIn.vue'),
		meta: { title: 'Registro - Delicias Norte' }
	},
	{
		path: '/registro-completo/',
		name: 'SignInComplete',
		component: () => import('../views/SignInComplete.vue'),
		meta: { title: 'Registro Exitoso - Delicias Norte' }
	},
	{
		path: '/admin/',
		name: 'LoginAdmin',
		component: () => import('../views/LoginAdmin.vue'),
		meta: { title: 'Administrador - Delicias Norte' }
	},
	{
		path: '/admin-panel/',
		name: 'AdminPanel',
		component: () => import('../views/AdminPanel.vue'),
		meta: { title: 'Administrador - Delicias Norte' }
	}
];

const router = new VueRouter({
	mode: 'history',
	base: '/',
	routes
});

export default router;
