import Vue from 'vue';
import Vuex from 'vuex';

Vue.use(Vuex);

export default new Vuex.Store({
	state: {
		dialogAccess: false,
		dialogEditAccount: false,
		alertDialogAccessType: 'error',
		alertDialogAccessMsg: '',
		alertDialogAccessStyle: {
			display: 'none'
		},
		dialogCart: false,
		dialogLoginSuccess: false,
		dialogLogOutSuccess: false,
		choosen_products: [],
		productsInCart: [],
		userLogin: false,
	},
	mutations: {
		changeStateDialogEditAccount(state, status) {
			state.dialogEditAccount = status;
		},

		changeStateDialogAccess(state, status) {
			state.dialogAccess = status;
		},

		changeStateAlertDialogAccessType(state, type) {
			state.alertDialogAccessType = type;
		},

		changeStateAlertDialogAccessMsg(state, msg) {
			state.alertDialogAccessMsg = msg;
		},

		changeStateAlertDialogAccessStyle(state, style) {
			if (state.alertDialogAccessType === 'error') {
				style === 'block'
					? (state.alertDialogAccessStyle.display = 'flex')
					: (state.alertDialogAccessStyle.display = 'none');
				state.alertDialogAccessStyle.alignItems = 'center';
				state.alertDialogAccessStyle.JustifyContent = 'center';
				state.alertDialogAccessStyle.height = '60px';
				document.getElementsByClassName(
					'alert-dialog-access-msg-icon'
				)[0].style.position = 'absolute';
				document.getElementsByClassName(
					'alert-dialog-access-msg-icon'
				)[0].style.marginLeft = '74.5%';
				return;
			}

			if (
				document.getElementsByClassName(
					'alert-dialog-access-msg-icon'
				)[0] !== undefined
			) {
				document.getElementsByClassName(
					'alert-dialog-access-msg-icon'
				)[0].style.position = 'unset';
			}

			if (
				document.getElementsByClassName(
					'alert-dialog-access-msg-icon'
				)[0] !== undefined
			) {
				document.getElementsByClassName(
					'alert-dialog-access-msg-icon'
				)[0].style.marginLeft = '0';
			}

			state.alertDialogAccessStyle.height = 'unset';
			state.alertDialogAccessStyle.display = style;
		},

		changeStateDialogCart(state, status) {
			if (!state.userLogin) {
				state.dialogAccess = true;
				state.alertDialogAccessMsg =
					'Inicia Sesión en Delicias Nortes Online Para disfrutar de la modalidad de pedidos';
				state.alertDialogAccessType = 'info';
				state.alertDialogAccessStyle.display = 'block';
				return;
			}

			state.dialogCart = status;

			if (state.dialogCart === false) {
				for (let index = 0; index < state.productsInCart.length; ) {
					if (state.productsInCart[index].quantity === 0) {
						state.productsInCart.splice(index, 1);
					} else {
						index++;
					}
				}
			}
		},

		changeStateProductInCart(state, product_data) {
			if (state.productsInCart) {
				for (let product of state.productsInCart) {
					if (product_data.id === product.id) {
						product.quantity += 1;
						return;
					}
				}
			}
			state.productsInCart.unshift(product_data);
		},

		ChangeStateDialogLoginSuccess(state, status) {
			state.dialogLoginSuccess = status;
		},

		changeStateUserLogin(state, status) {
			state.userLogin = status;
		},

		changeStateDialogLogOutSuccess(state, status) {
			state.dialogLogOutSuccess = status;
		}
	},
	actions: {},
	modules: {}
});
